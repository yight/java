--测试表
create table g_test
(
   id                   int not null auto_increment,
   username             varchar(50) not null,
   password             varchar(50) not null,
   createdate           timestamp DEFAULT CURRENT_TIMESTAMP,
   primary key (id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;