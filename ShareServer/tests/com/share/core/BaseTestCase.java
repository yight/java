package com.share.core;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * 
* @ClassName:BaseTestCase
* @Package:  com.share.core 
* @Title:(必填)
* @Description:(用一句话描述该文件做什么)
* 
* @author: jessen.chen
* @date:   2012-6-8 下午01:10:45
* @version 1.0.0
*
 */
@ContextConfiguration(
		locations = {
				"classpath:spring/application-test.xml"
			}
		)
@RunWith(SpringJUnit4ClassRunner.class)
public abstract class BaseTestCase {
	
}
