/**  
* 系统项目名称  
* com.share.core.dao.system  
* TestDaoTest.java  
*  
* 2012XX公司-版权所有  
*
* @author: jessen.chen
* @date:   2012-6-8 下午01:09:13
* @version 1.0.0
*   
*/
package com.share.core.dao.system;

import static org.junit.Assert.fail;

import java.util.Date;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.share.core.BaseTestCase;

/**
 * @ClassName:TestDaoTest
 * @Package:  com.share.core.dao.system 
 * @Title:(必填)
 * @Description:(用一句话描述该文件做什么)
 * 
 * @author: jessen.chen
 * @date:   2012-6-8 下午01:09:13
 * @version 1.0.0
 *   
 */
public class TestDaoTest extends BaseTestCase {
	private TestDao testDao;

	@Autowired
	public void setTestDao(TestDao testDao) {
		this.testDao = testDao;
	}

	/**
	 * @Description:(用一句话描述该文件做什么)
	 * 
	 * @throws java.lang.Exception void
	 * 
	 * @author: jessen.chen
	 * @date:   2012-6-8 下午01:09:13
	 * @version 1.0.0
	 *   
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @Description:(用一句话描述该文件做什么)
	 * 
	 * @throws java.lang.Exception void
	 * 
	 * @author: jessen.chen
	 * @date:   2012-6-8 下午01:09:13
	 * @version 1.0.0
	 *   
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @Description:(用一句话描述该文件做什么)
	 * 
	 * @throws java.lang.Exception void
	 * 
	 * @author: jessen.chen
	 * @date:   2012-6-8 下午01:09:13
	 * @version 1.0.0
	 *   
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @Description:(用一句话描述该文件做什么)
	 * 
	 * @throws java.lang.Exception void
	 * 
	 * @author: jessen.chen
	 * @date:   2012-6-8 下午01:09:13
	 * @version 1.0.0
	 *   
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link com.share.core.dao.system.TestDao#add(com.share.core.pojos.system.Test)}.
	 */
	@Test
	public void testAdd() {
//		fail("Not yet implemented");
		com.share.core.pojos.system.Test m = new com.share.core.pojos.system.Test();
		m.setUsername("jessen");
		m.setPassword("111111");
		m.setCreatedate(new Date());
		testDao.add(m);
	}

	/**
	 * Test method for {@link com.share.core.dao.system.TestDao#update(com.share.core.pojos.system.Test)}.
	 */
	@Test
	public void testUpdate() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.share.core.dao.system.TestDao#delete(int)}.
	 */
	@Test
	public void testDelete() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.share.core.dao.system.TestDao#get(int)}.
	 */
	@Test
	public void testGet() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.share.core.dao.system.TestDao#findTestList()}.
	 */
	@Test
	public void testFindTestList() {
		fail("Not yet implemented");
	}

}
