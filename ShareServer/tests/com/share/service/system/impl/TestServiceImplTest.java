package com.share.service.system.impl;

import static org.junit.Assert.*;

import java.util.Date;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.share.core.BaseTestCase;
import com.share.service.system.TestService;

public class TestServiceImplTest extends BaseTestCase {
	
	private TestService testService;

	@Autowired
	public void setTestService(TestService testService) {
		this.testService = testService;
	}

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testInsertTest() {
//		fail("Not yet implemented");
		com.share.core.pojos.system.Test m = new com.share.core.pojos.system.Test();
		m.setUsername("jessen");
		m.setPassword("111111");
		m.setCreatedate(new Date());
		
		try {
			testService.insertTest(m);
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
		}
	}
	
//	@Test
	public void testJson() {
		com.share.core.pojos.system.Test m = new com.share.core.pojos.system.Test();
		m.setUsername("jessen");
		m.setPassword("111111");
		m.setCreatedate(new Date());
		testService.jsonTest(m);
		
	}

}
