package com.share.core.util.sql;

import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.share.core.dao.system.SysColumnDao;

/**
 * 
 * @ClassName:JavaGenerator
 * @Package: com.grandison.gtodo.tools
 * @Title:pojos sql 生成类
 * @Description:
 * 
 * @author: jessen
 * @date: 2011-10-12 上午10:38:32
 * @version 1.0.0
 * 
 */
public class JavaGenerator {
	private final static String DATABASE_NAME = "share";
	private final static String TABLE_NAME = "g_test";

	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext(new String[] { "spring/application-test.xml" });

		SysColumnDao sysColumnDao = (SysColumnDao) context.getBean("sysColumnDao");
		List list = sysColumnDao.findList(DATABASE_NAME, TABLE_NAME);
		System.out.println(PojosHelper.getPojosStr(list));
		System.out.println(PojosHelper.getPojosMethod(list));

		System.out.println(DaoHelper.getDaoInsertStr(TABLE_NAME, list));
		System.out.println(DaoHelper.getDaoUpdateStr(TABLE_NAME, list));
		System.out.println(DaoHelper.getDaoRowMapper(list));
	}
}
