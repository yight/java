package com.share.core.util.sql;

import java.util.Iterator;
import java.util.List;

import com.share.core.pojos.system.SysColumn;

/**
 * 
 * @ClassName:PojosHelper
 * @Package: com.grandison.core.tools
 * @Title:(pojos生成帮助类)
 * @Description:(用一句话描述该文件做什么)
 * 
 * @author: jessen
 * @date: 2011-6-8 下午02:56:46
 * @version 1.0.0
 * 
 */
public class PojosHelper {
	/**
	 * 
	 * @Description:(通过字段列表生成pojos中的属性)
	 * 
	 * @param sqllist
	 * @return String
	 * 
	 * @author: jessen
	 * @date: 2011-6-8 下午05:42:34
	 * @version 1.0.0
	 * 
	 */
	public static String getPojosStr(List<SysColumn> sqllist) {
		StringBuffer sb = new StringBuffer();
		for (Iterator iterator = sqllist.iterator(); iterator.hasNext();) {
			// Map<String, Object> map = (Map<String, Object>) iterator.next();
			SysColumn sysColumn = (SysColumn) iterator.next();
			if (sysColumn.getColumnType().equals("int") || sysColumn.getColumnType().equals("smallint")) {
				sb.append("private int ").append(sysColumn.getColumnCode()).append(";").append("\r\n");
			} else if (sysColumn.getColumnType().equals("varchar") || sysColumn.getColumnType().equals("text")) {
				sb.append("private String ").append(sysColumn.getColumnCode()).append(";").append("\r\n");
			} else if (sysColumn.getColumnType().equals("timestamp")) {
				sb.append("private Date ").append(sysColumn.getColumnCode()).append(";").append("\r\n");
			} else if (sysColumn.getColumnType().equals("number") || sysColumn.getColumnType().equals("double")) {
				sb.append("private double ").append(sysColumn.getColumnCode()).append(";").append("\r\n");
			}
		}
		// System.out.println(sb);
		return sb.toString();
	}

	/**
	 * 
	 * @Description:(通过参数列表生成pojos中的方法)
	 * 
	 * @param sqllist
	 * @return String
	 * 
	 * @author: jessen
	 * @date: 2011-6-8 下午05:43:13
	 * @version 1.0.0
	 * 
	 */
	public static String getPojosMethod(List<SysColumn> sqllist) {
		StringBuffer sb = new StringBuffer();
		for (Iterator iterator = sqllist.iterator(); iterator.hasNext();) {
			SysColumn sysColumn = (SysColumn) iterator.next();
			sb.append("public ");
			if (sysColumn.getColumnType().equals("int") || sysColumn.getColumnType().equals("smallint")) {
				sb.append("int");
			} else if (sysColumn.getColumnType().equals("varchar") || sysColumn.getColumnType().equals("text")) {
				sb.append("String");
			} else if (sysColumn.getColumnType().equals("timestamp")) {
				sb.append("Date");
			} else if (sysColumn.getColumnType().equals("number") || sysColumn.getColumnType().equals("double")) {
				sb.append("double");
			}
			sb.append(" get").append(sysColumn.getColumnCode().toString().substring(0, 1).toUpperCase())
					.append(sysColumn.getColumnCode().toString().substring(1)).append("() {").append("\r\n");
			sb.append("return ").append(sysColumn.getColumnCode()).append(";\r\n");
			sb.append("}\r\n\r\n");
			sb.append("public void set").append(sysColumn.getColumnCode().toString().substring(0, 1).toUpperCase())
					.append(sysColumn.getColumnCode().toString().substring(1)).append("(");
			if (sysColumn.getColumnType().equals("int") || sysColumn.getColumnType().equals("smallint")) {
				sb.append("int ");
			} else if (sysColumn.getColumnType().equals("varchar") || sysColumn.getColumnType().equals("text")) {
				sb.append("String ");
			} else if (sysColumn.getColumnType().equals("timestamp")) {
				sb.append("Date ");
			} else if (sysColumn.getColumnType().equals("number") || sysColumn.getColumnType().equals("double")) {
				sb.append("double ");
			}
			sb.append(sysColumn.getColumnCode()).append(") {").append("\r\n");
			sb.append("this.").append(sysColumn.getColumnCode()).append(" = ").append(sysColumn.getColumnCode()).append(";\r\n");
			sb.append("}").append("\r\n").append("\r\n");
		}
		// System.out.println(sb.toString());
		return sb.toString();
	}
}
