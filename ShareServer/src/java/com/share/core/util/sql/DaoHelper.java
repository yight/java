package com.share.core.util.sql;

import java.util.Iterator;
import java.util.List;

import com.share.core.pojos.system.SysColumn;

/**
 * 
* @ClassName:DaoHelper
* @Package:  com.share.core.util.sql 
* @Title:(dao生成帮助类)
* @Description:(用一句话描述该文件做什么)
* 
* @author: jessen.chen
* @date:   2012-6-8 下午12:07:09
* @version 1.0.0
*
 */
public class DaoHelper {
	/**
	 * 
	* @Description:(生成insert语句)
	* 
	* @param tablename
	* @param sqllist
	* @return String
	* 
	* @author: jessen.chen
	* @date:   2012-6-8 下午12:07:19
	* @version 1.0.0
	*
	 */
	public static String getDaoInsertStr(String tablename, List<SysColumn> sqllist) {
		StringBuffer sb = new StringBuffer();
		sb.append("insert into ");
		sb.append(tablename);
		sb.append("(");
		StringBuffer temp = new StringBuffer();
		temp.append(" values(");
		int i = 0;
		for (Iterator iterator = sqllist.iterator(); iterator.hasNext();) {
			// Map<String, Object> map = (Map<String, Object>) iterator.next();
			SysColumn sysColumn = (SysColumn) iterator.next();
			sb.append(sysColumn.getColumnCode());
			if (!sysColumn.getColumnKey().equals("PRI")) {
				temp.append(":").append(sysColumn.getColumnCode());
			} else {
				temp.append(0);
			}
			i++;
			if (i < sqllist.size()) {
				temp.append(", ");
				sb.append(", ");
			}
		}
		temp.append(")");
		sb.append(")");
		sb.append(temp.toString());
		// System.out.println("sb:" + sb);
		return sb.toString();
	}

	/**
	 * 
	* @Description:(生成update语句)
	* 
	* @param tablename
	* @param sqllist
	* @return String
	* 
	* @author: jessen.chen
	* @date:   2012-6-8 下午12:07:33
	* @version 1.0.0
	*
	 */
	public static String getDaoUpdateStr(String tablename, List<SysColumn> sqllist) {
		StringBuffer sb = new StringBuffer();
		sb.append("update ").append(tablename).append(" set ");
		int i = 0;
		StringBuffer wheresqlSb = new StringBuffer();
		for (Iterator iterator = sqllist.iterator(); iterator.hasNext();) {
			// Map<String, Object> map = (Map<String, Object>) iterator.next();
			SysColumn sysColumn = (SysColumn) iterator.next();
			if (!sysColumn.getColumnKey().equals("PRI")) {
				sb.append(sysColumn.getColumnCode()).append("=:").append(sysColumn.getColumnCode());
			} else {
				wheresqlSb.append(" where ").append(sysColumn.getColumnCode()).append("=").append(":").append(sysColumn.getColumnCode());
			}
			i++;
			if (i < sqllist.size() && i > 1) {
				sb.append(",");
			}
		}
		sb.append(wheresqlSb);
		// System.out.println("sb:" + sb);
		return sb.toString();
	}

	/**
	 * 
	* @Description:(生成rowMapper类信息)
	* 
	* @param sqllist
	* @return String
	* 
	* @author: jessen.chen
	* @date:   2012-6-8 下午12:07:46
	* @version 1.0.0
	*
	 */
	public static String getDaoRowMapper(List<SysColumn> sqllist) {
		StringBuffer sb = new StringBuffer();
		for (Iterator iterator = sqllist.iterator(); iterator.hasNext();) {
			StringBuffer columnSb = new StringBuffer();
			// Map<String, Object> map = (Map<String, Object>) iterator.next();
			SysColumn sysColumn = (SysColumn) iterator.next();
			columnSb.append(sysColumn.getColumnCode().toString().substring(0, 1).toUpperCase()).append(
					sysColumn.getColumnCode().toString().substring(1));
			sb.append("o.set").append(columnSb).append("(rs.get");
			if (sysColumn.getColumnType().equals("int") || sysColumn.getColumnType().equals("smallint")) {
				sb.append("Int(\"");
			} else if (sysColumn.getColumnType().equals("varchar") || sysColumn.getColumnType().equals("text") || sysColumn.getColumnType().equals("char")) {
				sb.append("String(\"");
			} else if (sysColumn.getColumnType().equals("timestamp")) {
				sb.append("Timestamp(\"");
			} else if (sysColumn.getColumnType().equals("number")) {
				sb.append("Double(\"");
			}
			sb.append(sysColumn.getColumnCode()).append("\"));\r\n");
		}
		// System.out.println("sb:" + sb);
		return sb.toString();
	}
}
