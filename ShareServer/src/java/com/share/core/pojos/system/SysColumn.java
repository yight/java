package com.share.core.pojos.system;

/**
 * 
* @ClassName:SysColumn
* @Package:  com.share.core.pojos.system 
* @Title:(系统字段类)
* @Description:(用一句话描述该文件做什么)
* 
* @author: jessen.chen
* @date:   2012-6-8 下午12:08:36
* @version 1.0.0
*
 */
public class SysColumn {
	private int id;// ID
	private String columnKey;// 是否为主键
	private String columnName;// 字段名称
	private String columnCode;// 字段符号
	private String columnType;// tinyint,smallint,int,bigint,varchar
	private int status;// 状态

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getColumnKey() {
		return columnKey;
	}

	public void setColumnKey(String columnKey) {
		this.columnKey = columnKey;
	}

	public String getColumnName() {
		return columnName;
	}

	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}

	public String getColumnCode() {
		return columnCode;
	}

	public void setColumnCode(String columnCode) {
		this.columnCode = columnCode;
	}

	public String getColumnType() {
		return columnType;
	}

	public void setColumnType(String columnType) {
		this.columnType = columnType;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "SysColumn [id=" + id + ", columnKey=" + columnKey + ", columnName=" + columnName + ", columnCode=" + columnCode + ", columnType="
				+ columnType + ", status=" + status + "]";
	}
}
