package com.share.core.pojos.system;

import java.io.Serializable;
import java.util.Date;

/**
 * 
* @ClassName:Test
* @Package:  com.share.core.pojos.system 
* @Title:(测试类)
* @Description:(用一句话描述该文件做什么)
* 
* @author: jessen.chen
* @date:   2012-6-8 下午12:44:36
* @version 1.0.0
*
 */
public class Test implements Serializable {

	/**
	 * serialVersionUID:TODO（用一句话描述这个变量表示什么）
	 * 
	 * @author: jessen.chen
	 * @date: 2012-6-8 下午12:44:21
	 * @version 1.0.0
	 * 
	 */

	private static final long serialVersionUID = 1L;

	private int id;
	private String username;
	private String password;
	private Date createdate;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Date getCreatedate() {
		return createdate;
	}

	public void setCreatedate(Date createdate) {
		this.createdate = createdate;
	}

	@Override
	public String toString() {
		return "Test [id=" + id + ", username=" + username + ", password=" + password + ", createdate=" + createdate
				+ "]";
	}
}
