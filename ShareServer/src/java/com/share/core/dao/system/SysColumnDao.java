package com.share.core.dao.system;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.share.core.framework.BaseDaoImpl;
import com.share.core.pojos.system.SysColumn;

/**
 * 
* @ClassName:SysColumnDao
* @Package:  com.share.core.dao.system 
* @Title:(系统字段DAO操作)
* @Description:(用一句话描述该文件做什么)
* 
* @author: jessen.chen
* @date:   2012-6-8 下午12:09:48
* @version 1.0.0
*
 */
@Component
public class SysColumnDao extends BaseDaoImpl {
	// 通过数据库和表取得字段列表
	final static String SQL_LIST = "select * from information_schema.columns where table_schema = :tableSchema and table_name = :tableName";

	/**
	 * 
	* @Description:(通过数据库与表取得表中字段信息)
	* 
	* @param databaseName
	* @param tableName
	* @return List<SysColumn>
	* 
	* @author: jessen.chen
	* @date:   2012-6-8 下午12:10:02
	* @version 1.0.0
	*
	 */
	public List<SysColumn> findList(String databaseName, String tableName) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("tableSchema", databaseName);
		map.put("tableName", tableName);
		return dao.getJdbcTemplate().query(SQL_LIST, map, new SysColumnMapper());
	}

	protected class SysColumnMapper implements RowMapper {
		public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
			SysColumn o = new SysColumn();
			o.setId(rs.getInt("ordinal_position"));
			o.setColumnCode(rs.getString("column_name"));
			o.setColumnName(rs.getString("column_comment"));
			o.setColumnType(rs.getString("data_type"));
			o.setColumnKey(rs.getString("column_key"));

			return o;
		}
	}
}
