package com.share.core.dao.system.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.share.core.dao.system.TestDao;
import com.share.core.framework.BaseDaoImpl;
import com.share.core.pojos.system.Test;

/**
 * 
 * @ClassName:TestDao
 * @Package: com.share.core.dao.system
 * @Title:(测试类DAO实现类)
 * @Description:(用一句话描述该文件做什么)
 * 
 * @author: jessen.chen
 * @date: 2012-6-8 下午12:45:18
 * @version 1.0.0
 * 
 */
@Component
public class TestDaoImpl extends BaseDaoImpl implements TestDao {
	// 新增
	final static String SQL_INSERT = "insert into g_test(id, username, password, createdate) values(0, :username, :password, :createdate)";
	// 修改
	final static String SQL_UPDATE = "update g_test set username=:username,password=:password,createdate=:createdate where id=:id";
	// 删除
	final static String SQL_DELETE = "delete from g_system_error_log where id = ?";
	// 根据ID查找
	final static String SQL_GET = "select * from g_system_error_log where id = ?";
	// 列表查询
	final static String SQL_LIST = "select * from g_system_error_log order by id";

	/**
	 * 
	 * @Description:(新增)
	 * 
	 * @param m
	 * @return int
	 * 
	 * @author: jessen.chen
	 * @date: 2012-6-8 下午12:48:14
	 * @version 1.0.0
	 * 
	 */
	@Override
	public int add(Test m) {
		int i = dao.getJdbcTemplate().updateObject(SQL_INSERT, m);
		return i;
	}

	/**
	 * 
	 * @Description:(修改)
	 * 
	 * @param m
	 * @return int
	 * 
	 * @author: jessen.chen
	 * @date: 2012-6-8 下午12:48:06
	 * @version 1.0.0
	 * 
	 */
	@Override
	public int update(Test m) {
		int i = dao.getJdbcTemplate().updateObject(SQL_UPDATE, m);
		return i;
	}

	/**
	 * 
	 * @Description:(删除)
	 * 
	 * @param id
	 * @return boolean
	 * 
	 * @author: jessen.chen
	 * @date: 2012-6-8 下午12:48:03
	 * @version 1.0.0
	 * 
	 */
	@Override
	public boolean delete(int id) {
		return dao.getJdbcTemplate().update(SQL_DELETE, new Object[] { id }) > 0;
	}

	/**
	 * 
	 * @Description:(通过ID查询)
	 * 
	 * @param id
	 * @return Test
	 * 
	 * @author: jessen.chen
	 * @date: 2012-6-8 下午12:47:59
	 * @version 1.0.0
	 * 
	 */
	@Override
	public Test get(int id) {
		Test o = (Test) dao.getJdbcTemplate().queryForObject(SQL_GET, new Object[] { id }, new TestMapper());
		return o;
	}

	/**
	 * 
	 * @Description:(查询全部)
	 * 
	 * @return List<Test>
	 * 
	 * @author: jessen.chen
	 * @date: 2012-6-8 下午12:47:54
	 * @version 1.0.0
	 * 
	 */
	@Override
	public List<Test> findTestList() {
		List<Test> list = dao.getJdbcTemplate().query(SQL_LIST, new TestMapper());
		return list;
	}

	protected class TestMapper implements RowMapper {

		@Override
		public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
			Test o = new Test();
			o.setId(rs.getInt("id"));
			o.setUsername(rs.getString("username"));
			o.setPassword(rs.getString("password"));
			o.setCreatedate(rs.getTimestamp("createdate"));

			return o;
		}
	}
}
