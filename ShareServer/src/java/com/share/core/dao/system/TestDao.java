package com.share.core.dao.system;

import java.util.List;

import org.springframework.stereotype.Component;

import com.share.core.pojos.system.Test;

/**
 * 
 * @ClassName:TestDao
 * @Package: com.share.core.dao.system
 * @Title:(测试类DAO接口)
 * @Description:(用一句话描述该文件做什么)
 * 
 * @author: jessen.chen
 * @date: 2012-6-8 下午12:45:18
 * @version 1.0.0
 * 
 */
public interface TestDao {

	/**
	 * 
	 * @Description:(新增)
	 * 
	 * @param m
	 * @return int
	 * 
	 * @author: jessen.chen
	 * @date: 2012-6-8 下午12:48:14
	 * @version 1.0.0
	 * 
	 */
	public int add(Test m);

	/**
	 * 
	 * @Description:(修改)
	 * 
	 * @param m
	 * @return int
	 * 
	 * @author: jessen.chen
	 * @date: 2012-6-8 下午12:48:06
	 * @version 1.0.0
	 * 
	 */
	public int update(Test m);

	/**
	 * 
	 * @Description:(删除)
	 * 
	 * @param id
	 * @return boolean
	 * 
	 * @author: jessen.chen
	 * @date: 2012-6-8 下午12:48:03
	 * @version 1.0.0
	 * 
	 */
	public boolean delete(int id);

	/**
	 * 
	 * @Description:(通过ID查询)
	 * 
	 * @param id
	 * @return Test
	 * 
	 * @author: jessen.chen
	 * @date: 2012-6-8 下午12:47:59
	 * @version 1.0.0
	 * 
	 */
	public Test get(int id);

	/**
	 * 
	 * @Description:(查询全部)
	 * 
	 * @return List<Test>
	 * 
	 * @author: jessen.chen
	 * @date: 2012-6-8 下午12:47:54
	 * @version 1.0.0
	 * 
	 */
	public List<Test> findTestList();
}
