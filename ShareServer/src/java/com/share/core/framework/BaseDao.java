package com.share.core.framework;

import java.util.Map;

import org.springframework.jdbc.core.RowMapper;

/**
 * 
* @ClassName:BaseDao
* @Package:  com.share.core.framework 
* @Title:(数据库相关接口)
* @Description:(用一句话描述该文件做什么)
* 
* @author: jessen.chen
* @date:   2012-6-1 上午11:52:13
* @version 1.0.0
*
 */
public interface BaseDao {
	/**
	 * 
	 * @Description:(springJDBC模板操作类)
	 * 
	 * @return MyJdbcTemplate
	 * 
	 * @author: jessen
	 * @date: 2012-6-1 下午05:25:13
	 * @version 1.0.0
	 * 
	 */
	public MyJdbcTemplate getJdbcTemplate();

	/**
	 * 
	 * @Description:(通过sql,当前页数,每页页数查询得到Page对象)
	 * 
	 * @param sql
	 * @param pageid
	 * @param size
	 * @return Page
	 * 
	 * @author: jessen
	 * @date: 2012-6-1 下午05:26:47
	 * @version 1.0.0
	 * 
	 */
	public Page queryForPage(String sql, int pageid, int size);

	/**
	 * 
	 * @Description:(通过sql,当前页数,每页页数,RowMapper查询得到Page对象)
	 * 
	 * @param sql
	 * @param pageid
	 * @param size
	 * @param rowMapper
	 * @return Page
	 * 
	 * @author: jessen
	 * @date: 2012-6-1 下午11:47:47
	 * @version 1.0.0
	 * 
	 */
	public Page queryForPage(String sql, RowMapper rowMapper, int pageid, int size);

	/**
	 * 
	 * @Description:(通过sql,paramMap,当前页数,每页页数查询得到Page对象)
	 * 
	 * @param sql
	 * @param paramMap
	 * @param pageid
	 * @param size
	 * @return Page
	 * 
	 * @author: jessen
	 * @date: 2012-6-1 下午05:27:31
	 * @version 1.0.0
	 * 
	 */
	public Page queryForPage(String sql, Map<String, Object> paramMap, int pageid, int size);

	/**
	 * 
	 * @Description:(通过sql,对象数组,当前页数,每页页数查询得到Page对象)
	 * 
	 * @param sql
	 * @param obj
	 * @param pageid
	 * @param size
	 * @return Page
	 * 
	 * @author: jessen
	 * @date: 2012-6-1 下午05:28:07
	 * @version 1.0.0
	 * 
	 */
	public Page queryForPage(String sql, Object[] obj, int pageid, int size);

	/**
	 * 
	 * @Description:(通过sql,paramMap,当前页数,rowmapper,每页页数查询得到Page对象)
	 * 
	 * @param sql
	 * @param paramMap
	 * @param rowMapper
	 * @param pageid
	 * @param size
	 * @return Page
	 * 
	 * @author: jessen
	 * @date: 2012-6-1 下午05:28:26
	 * @version 1.0.0
	 * 
	 */
	public Page queryForPage(String sql, Map<String, Object> paramMap, RowMapper rowMapper, int pageid, int size);

	/**
	 * 
	 * @Description:(通过sql,对象数组,当前页数,rowmapper,每页页数查询得到Page对象)
	 * 
	 * @param sql
	 * @param obj
	 * @param rowMapper
	 * @param pageid
	 * @param size
	 * @return Page
	 * 
	 * @author: jessen
	 * @date: 2012-6-1 下午05:28:56
	 * @version 1.0.0
	 * 
	 */
	public Page queryForPage(String sql, Object[] obj, RowMapper rowMapper, int pageid, int size);
}