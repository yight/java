package com.share.core.framework;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * 
 * @ClassName:BaseService
 * @Package: com.share.core.framework
 * @Title:(业务层基类)
 * @Description:(用一句话描述该文件做什么)
 * 
 * @author: jessen.chen
 * @date: 2012-6-1 上午11:52:32
 * @version 1.0.0
 * 
 */
public abstract class BaseService {

	// get log4j handler
	protected static final Logger logger = Logger.getLogger(BaseService.class);
	protected static ObjectMapper objectMapper;
	protected static JsonGenerator jsonGenerator;
	static {
		objectMapper = new ObjectMapper();
	}

	/**
	 * 
	 * @Description:(Object转json 字符串)
	 * 
	 * @param o
	 * @return
	 * @throws Exception
	 *             Object
	 * 
	 * @author: jessen.chen
	 * @date: 2012-6-8 下午04:40:41
	 * @version 1.0.0
	 * 
	 */
	public static String beanToJson(Object o) throws Exception {
		StringWriter writer = null;
		JsonGenerator gen = null;
		try {
			writer = new StringWriter();
			gen = objectMapper.getJsonFactory().createJsonGenerator(writer);
			objectMapper.writeValue(gen, o);

			return writer.toString();
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if (gen != null) {
					gen.close();
				}
				if (writer != null) {
					writer.close();
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return null;
	}

	/**
	 * 
	 * @Description:(json 字符串转Object)
	 * 
	 * @param json
	 * @param cls
	 * @return
	 * @throws Exception
	 *             Object
	 * 
	 * @author: jessen.chen
	 * @date: 2012-6-8 下午04:41:09
	 * @version 1.0.0
	 * 
	 */
	public static Object jsonToBean(String json, Class<?> cls) throws Exception {
		Object vo = objectMapper.readValue(json, cls);
		return vo;
	}

	/**
	 * 
	 * @Description:(json 字符串转List)
	 * 
	 * @param json
	 * @param cls
	 * @return
	 * @throws Exception
	 *             List<?>
	 * 
	 * @author: jessen.chen
	 * @date: 2012-7-4 下午01:56:11
	 * @version 1.0.0
	 * 
	 */
	public static List<?> jsonToList(String json, Class<?> cls) throws Exception {
		Object[] objArr = (Object[]) objectMapper.readValue(json, cls);

		return Arrays.asList(objArr);
	}
}
