package com.share.core.framework;

import java.util.List;

/**
 * 
 * @ClassName:Page
 * @Package: com.share.core.framework
 * @Title:(分页对象)
 * @Description:(用一句话描述该文件做什么)
 * 
 * @author: jessen.chen
 * @date: 2012-6-1 上午11:57:37
 * @version 1.0.0
 * 
 */
public class Page {
	private int currentPage;// 当前页数
	private int pageSize;// 每页记录数
	private int totalCount;// 总记录数
	private List<Object> results;// 记录信息

	public Page() {
	}

	public Page(int currentPage, int pageSize, int totalCount, List<Object> results) {
		this.currentPage = currentPage;
		this.pageSize = pageSize;
		this.totalCount = totalCount;
		this.results = results;
	}

	/**
	 * 
	 * @Description:(得到总页数)
	 * 
	 * @return int
	 * 
	 * @author: jessen
	 * @date: 2012-6-1 下午12:10:56
	 * @version 1.0.0
	 * 
	 */
	public int getTotalPageNumber() {
		if (totalCount < 1 || pageSize == 0)
			return 0;
		if ((totalCount > pageSize) && (totalCount % pageSize != 0)) {
			return totalCount / pageSize + 1;
		}
		if ((totalCount > pageSize) && (totalCount % pageSize == 0)) {
			return totalCount / pageSize;
		}
		return 1;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	public List<Object> getResults() {
		return results;
	}

	public void setResults(List<Object> results) {
		this.results = results;
	}
}