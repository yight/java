package com.share.core.framework;

import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;

/**
 * 
 * @ClassName:BaseMysqlDao
 * @Package: com.share.core.framework
 * @Title:(mysql数据库操作接口实现类)
 * @Description:(用一句话描述该文件做什么)
 * 
 * @author: jessen.chen
 * @date: 2012-6-1 上午11:51:48
 * @version 1.0.0
 * 
 */
@Component
public class BaseMysqlDao implements BaseDao {
	private boolean debug = false;// debug标示符

	// get log4j handler
	private static final Logger logger = Logger.getLogger(BaseMysqlDao.class);

	private MyJdbcTemplate myJdbcTemplate;// 整合JDBC模板类

	private DataSource dataSource;// 数据源

	protected PlatformTransactionManager transactionManager;

	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	@Autowired
	public void setTransactionManager(PlatformTransactionManager transactionManager) {
		this.transactionManager = transactionManager;
	}

	// @Autowired
	public void setDebug(boolean debug) {
		this.debug = debug;
	}

	@Override
	public MyJdbcTemplate getJdbcTemplate() {
		if (myJdbcTemplate == null) {
			myJdbcTemplate = new MyJdbcTemplate(dataSource, debug);
		}
		return myJdbcTemplate;
	}

	/**
	 * 
	 * @Description:(通过sql,pageid,size得到分页sql语句)
	 * 
	 * @param pageid
	 *            当前页数
	 * @param size
	 *            每页页数
	 * @return String
	 * 
	 * @author: jessen
	 * @date: 2012-6-1 下午02:17:02
	 * @version 1.0.0
	 * 
	 */
	public String getPagelistSql(String sql, int pageid, int size) {
		int start = (pageid - 1) * size + 1;
		StringBuffer sb = new StringBuffer();
		sb.append("select ta.* from (").append(sql).append(") ta limit ").append(start - 1).append(",").append(size);
		if (debug) {
			logger.info("sql:" + sb.toString());
		}
		return sb.toString();
	}

	/**
	 * 
	 * @Description:(通过sql,pageid,size得到分页sql语句)
	 * 
	 * @param sql
	 * @return String
	 * 
	 * @author: jessen
	 * @date: 2012-6-1 下午03:12:21
	 * @version 1.0.0
	 * 
	 */
	public String getTotalcountSql(String sql) {
		StringBuffer sb = new StringBuffer();
		sb.append("select count(*) from (").append(sql).append(") ta");
		return sb.toString();
	}

	/**
	 * 
	 * @Description:(通过sql,pageid,size得到分页列表)
	 * 
	 * @param pageid
	 *            当前页数
	 * @param size
	 *            每页页数
	 * @return String
	 * 
	 * @author: jessen
	 * @date: 2012-6-1 下午02:17:02
	 * @version 1.0.0
	 * 
	 */
	@Override
	public Page queryForPage(String sql, int pageid, int size) {
		Page page = new Page();
		List list = getJdbcTemplate().query(getPagelistSql(sql, pageid, size));
		page.setCurrentPage(pageid);
		page.setPageSize(size);
		page.setTotalCount(getJdbcTemplate().queryForInt(getTotalcountSql(sql)));
		page.setResults(list);
		return page;
	}

	/**
	 * 
	 * @Description:(通过sql,当前页数,每页页数,RowMapper查询得到Page对象)
	 * 
	 * @param sql
	 * @param pageid
	 * @param size
	 * @param rowMapper
	 * @return Page
	 * 
	 * @author: jessen
	 * @date: 2012-6-1 下午11:47:47
	 * @version 1.0.0
	 * 
	 */
	@Override
	public Page queryForPage(String sql, RowMapper rowMapper, int pageid, int size) {
		Page page = new Page();
		List list = getJdbcTemplate().query(getPagelistSql(sql, pageid, size), rowMapper);
		page.setCurrentPage(pageid);
		page.setPageSize(size);
		page.setTotalCount(getJdbcTemplate().queryForInt(getTotalcountSql(sql)));
		page.setResults(list);
		return page;
	}

	/**
	 * 
	 * @Description:(通过sql,pageid,size得到分页列表)
	 * 
	 * @param pageid
	 *            当前页数
	 * @param size
	 *            每页页数
	 * @return String
	 * 
	 * @author: jessen
	 * @date: 2012-6-1 下午02:17:02
	 * @version 1.0.0
	 * 
	 */
	@Override
	public Page queryForPage(String sql, Map<String, Object> paramMap, int pageid, int size) {
		Page page = new Page();
		List list = getJdbcTemplate().query(getPagelistSql(sql, pageid, size), paramMap);
		page.setCurrentPage(pageid);
		page.setPageSize(size);
		page.setTotalCount(getJdbcTemplate().queryForInt(getTotalcountSql(sql), paramMap));
		page.setResults(list);
		return page;
	}

	/**
	 * 
	 * @Description:(通过sql,pageid,size得到分页列表)
	 * 
	 * @param pageid
	 *            当前页数
	 * @param size
	 *            每页页数
	 * @return String
	 * 
	 * @author: jessen
	 * @date: 2012-6-1 下午02:17:02
	 * @version 1.0.0
	 * 
	 */
	@Override
	public Page queryForPage(String sql, Object[] obj, int pageid, int size) {
		Page page = new Page();
		List list = getJdbcTemplate().query(getPagelistSql(sql, pageid, size), obj);
		page.setCurrentPage(pageid);
		page.setPageSize(size);
		page.setTotalCount(getJdbcTemplate().queryForInt(getTotalcountSql(sql), obj));
		page.setResults(list);
		return page;
	}

	/**
	 * 
	 * @Description:(通过sql,map, pageid,size得到分页列表)
	 * 
	 * @param pageid
	 *            当前页数
	 * @param size
	 *            每页页数
	 * @return String
	 * 
	 * @author: jessen
	 * @date: 2012-6-1 下午02:17:02
	 * @version 1.0.0
	 * 
	 */
	@Override
	public Page queryForPage(String sql, Map<String, Object> paramMap, RowMapper rowMapper, int pageid, int size) {
		Page page = new Page();
		List list = getJdbcTemplate().query(getPagelistSql(sql, pageid, size), paramMap, rowMapper);
		page.setCurrentPage(pageid);
		page.setPageSize(size);
		page.setTotalCount(getJdbcTemplate().queryForInt(getTotalcountSql(sql), paramMap));
		page.setResults(list);
		return page;
	}

	/**
	 * 
	 * @Description:(通过sql,对象数组,Rowmapper,pageid,size得到分页列表)
	 * 
	 * @param pageid
	 *            当前页数
	 * @param size
	 *            每页页数
	 * @return String
	 * 
	 * @author: jessen
	 * @date: 2012-6-1 下午02:17:02
	 * @version 1.0.0
	 * 
	 */
	@Override
	public Page queryForPage(String sql, Object[] obj, RowMapper rowMapper, int pageid, int size) {
		Page page = new Page();
		List list = getJdbcTemplate().query(getPagelistSql(sql, pageid, size), obj, rowMapper);
		page.setCurrentPage(pageid);
		page.setPageSize(size);
		page.setTotalCount(getJdbcTemplate().queryForInt(getTotalcountSql(sql), obj));
		page.setResults(list);
		return page;
	}

	/**
	 * 
	 * @Description:(事务处理)
	 * 
	 * @param transCallback
	 * @return Object
	 * 
	 * @author: jessen
	 * @date: 2012-6-1 下午10:23:37
	 * @version 1.0.0
	 * 
	 */
	public Object execute(TransactionCallback transCallback) {
		return new TransactionTemplate(transactionManager).execute(transCallback);
	}
}