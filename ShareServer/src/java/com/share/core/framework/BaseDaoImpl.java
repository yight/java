package com.share.core.framework;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * 
* @ClassName:BaseDaoImpl
* @Package:  com.share.core.framework 
* @Title:(dao操作基类)
* @Description:(用一句话描述该文件做什么)
* 
* @author: jessen.chen
* @date:   2012-6-1 上午11:52:02
* @version 1.0.0
*
 */
public abstract class BaseDaoImpl {
	protected BaseDao dao;

	@Autowired
	protected void setBaseDao(BaseDao baseDao) {
		this.dao = baseDao;
	}
}
