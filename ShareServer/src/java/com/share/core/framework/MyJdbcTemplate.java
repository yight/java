package com.share.core.framework;

import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

/**
 * 
* @ClassName:MyJdbcTemplate
* @Package:  com.share.core.framework 
* @Title:(jdbcTemplate操作类)
* @Description:(用一句话描述该文件做什么)
* 
* @author: jessen.chen
* @date:   2012-6-1 上午11:54:49
* @version 1.0.0
*
 */
@Component
public class MyJdbcTemplate {
	private boolean debug = false;// debug标示符

	private DataSource ds = null;// 数据源
	// get log4j handler
	private static final Logger logger = Logger.getLogger(MyJdbcTemplate.class);

	public MyJdbcTemplate(DataSource dataSource) {
		this.ds = dataSource;
	}

	public MyJdbcTemplate(DataSource dataSource, boolean debug) {
		this.ds = dataSource;
		this.debug = debug;
	}

	@Autowired
	private void setDataSource(DataSource dataSource) {
		this.ds = dataSource;
	}

	public JdbcTemplate getJdbcTemplate() {
		if (debug) {
			logger.info("dataSource:" + ds);
		}
		return new JdbcTemplate(ds);
	}

	public NamedParameterJdbcTemplate getNameParameterJdbcTemplate() {
		if (debug) {
			logger.info("dataSource:" + ds);
		}
		return new NamedParameterJdbcTemplate(ds);
	}

	/**
	 * 
	 * @Description:(通过sql,对象数组修改数据库,返回记录ID)
	 * 
	 * @param sql
	 * @param args
	 * @return int
	 * 
	 * @author: jessen
	 * @date: 2012-6-1 上午12:17:02
	 * @version 1.0.0
	 * 
	 */
	public int update(String sql, Object[] args) throws DataAccessException {
		if (debug) {
			logger.info("sql:" + sql);
			logger.info("args:" + args);
		}
		long time = System.currentTimeMillis();
		int ret = getJdbcTemplate().update(sql, args);
		if (System.currentTimeMillis() - time > 1000 * 60) {
			logger.warn("sql update time is too long............start........");
			logger.warn(sql);
			logger.warn("sql update time is too long............end........");
		}
		return ret;
	}

	/**
	 * 
	 * @Description:(通过sql和obj修改数据库,返回记录ID)
	 * 
	 * @param sql
	 * @param object
	 * @return int
	 * 
	 * @author: jessen
	 * @date: 2012-6-1 下午05:00:41
	 * @version 1.0.0
	 * 
	 */
	public int updateObject(String sql, Object object) throws DataAccessException {
		if (debug) {
			logger.info("sql:" + sql);
			logger.info("object:" + object);
		}
		SqlParameterSource namedParam = new BeanPropertySqlParameterSource(object);
		KeyHolder keyHolder = new GeneratedKeyHolder();
		int ret = getNameParameterJdbcTemplate().update(sql, namedParam, keyHolder);
		if (ret <= 0)
			return ret;
		try {
			if (keyHolder.getKey() == null)
				return ret;
			ret = keyHolder.getKey().intValue();
			return ret;
		} catch (Exception e) {
			return ret;
		}
	}

	/**
	 * 
	 * @Description:(通过sql和map修改数据库,返回记录ID)
	 * 
	 * @param sql
	 * @param map
	 * @return int
	 * 
	 * @author: jessen
	 * @date: 2012-6-1 下午05:01:26
	 * @version 1.0.0
	 * 
	 */
	public int update(String sql, Map<String, Object> paramMap) throws DataAccessException {
		if (debug) {
			logger.info("sql:" + sql);
			logger.info("paramMap:" + paramMap);
		}
		long time = System.currentTimeMillis();
		int id = getNameParameterJdbcTemplate().update(sql, paramMap);
		if (System.currentTimeMillis() - time > 1000 * 60) {
			logger.warn("sql update time is too long............start........");
			logger.warn(sql);
			logger.warn("sql update time is too long............end........");
		}
		return id;
	}

	/**
	 * 
	 * @Description:(通过sql查询得到int类型)
	 * 
	 * @param sql
	 * @return int
	 * 
	 * @author: jessen
	 * @date: 2012-6-1 下午05:02:34
	 * @version 1.0.0
	 * 
	 */
	public int queryForInt(String sql) {
		if (debug) {
			logger.info("sql:" + sql);
		}
		long time = System.currentTimeMillis();
		int m = 0;
		try {
			m = getNameParameterJdbcTemplate().getJdbcOperations().queryForInt(sql);
			if (System.currentTimeMillis() - time > 1000 * 60) {
				logger.warn("sql query time is too long............start........");
				logger.warn(sql);
				logger.warn("sql query time is too long............end........");
			}
		} catch (EmptyResultDataAccessException e) {
		} catch (Exception e) {
			logger.info(e.getMessage());
		}
		return m;
	}

	/**
	 * 
	 * @Description:(通过sql和map查询得到int)
	 * 
	 * @param sql
	 * @param map
	 * @return int
	 * 
	 * @author: jessen
	 * @date: 2012-6-1 下午05:04:14
	 * @version 1.0.0
	 * 
	 */
	public int queryForInt(String sql, Map<String, Object> paramMap) {
		if (debug) {
			logger.info("sql:" + sql);
			logger.info("paramMap:" + paramMap);
		}
		long time = System.currentTimeMillis();
		int m = 0;
		try {
			m = getNameParameterJdbcTemplate().queryForInt(sql, paramMap);
			if (System.currentTimeMillis() - time > 1000 * 60) {
				logger.warn("sql query time is too long............start........");
				logger.warn(sql);
				logger.warn("sql query time is too long............end........");
			}
		} catch (EmptyResultDataAccessException e) {
		} catch (Exception e) {
			logger.info(e.getMessage());
		}
		return m;
	}

	/**
	 * 
	 * @Description:(通过sql和obj数组查询得到int)
	 * 
	 * @param sql
	 * @param objs
	 * @return int
	 * 
	 * @author: jessen
	 * @date: 2012-6-1 下午05:04:39
	 * @version 1.0.0
	 * 
	 */
	public int queryForInt(String sql, Object[] objs) {
		if (debug) {
			logger.info("sql:" + sql);
			logger.info("objs:" + objs);
		}
		int m = 0;
		long time = System.currentTimeMillis();
		try {
			m = getNameParameterJdbcTemplate().getJdbcOperations().queryForInt(sql, objs);
			if (System.currentTimeMillis() - time > 1000 * 60) {
				logger.warn("sql query time is too long............start........");
				logger.warn(sql);
				logger.warn("sql query time is too long............end........");
			}
		} catch (EmptyResultDataAccessException e) {
		} catch (Exception e) {
			logger.info(e.getMessage());
		}
		return m;
	}

	/**
	 * 
	 * @Description:(通过sql和rowmapper得到对象)
	 * 
	 * @param sql
	 * @param rowMapper
	 * @return Object
	 * 
	 * @author: jessen
	 * @date: 2012-6-1 下午05:05:09
	 * @version 1.0.0
	 * 
	 */
	public Object queryForObject(String sql, RowMapper<Object> rowMapper) throws DataAccessException {
		if (debug) {
			logger.info("sql:" + sql);
		}
		Object object = null;
		try {
			long time = System.currentTimeMillis();
			object = getNameParameterJdbcTemplate().getJdbcOperations().queryForObject(sql, rowMapper);
			if (System.currentTimeMillis() - time > 1000 * 60) {
				logger.warn("sql query time is too long............start........");
				logger.warn(sql);
				logger.warn("sql query time is too long............end........");
			}
		} catch (EmptyResultDataAccessException e) {
		}
		return object;
	}

	/**
	 * 
	 * @Description:(通过sql,paramMap和rowmapper得到对象)
	 * 
	 * @param sql
	 * @param paramMap
	 * @param rowMapper
	 * @return Object
	 * 
	 * @author: jessen
	 * @date: 2012-6-1 下午05:05:48
	 * @version 1.0.0
	 * 
	 */
	public Object queryForObject(String sql, Map<String, Object> paramMap, RowMapper<Object> rowMapper)
			throws DataAccessException {
		if (debug) {
			logger.info("sql:" + sql);
			logger.info("paramMap:" + paramMap);
		}

		Object object = null;
		try {
			long time = System.currentTimeMillis();
			object = getNameParameterJdbcTemplate().queryForObject(sql, paramMap, rowMapper);
			if (System.currentTimeMillis() - time > 1000 * 60) {
				logger.warn("sql query time is too long............start........");
				logger.warn(sql);
				logger.warn("sql query time is too long............end........");
			}
		} catch (EmptyResultDataAccessException e) {
		}
		return object;
	}

	/**
	 * 
	 * @Description:(通过sql,对象数组和rowmapper得到对象)
	 * 
	 * @param sql
	 * @param objs
	 * @param rowMapper
	 * @return Object
	 * 
	 * @author: jessen
	 * @date: 2012-6-1 下午05:05:48
	 * @version 1.0.0
	 * 
	 */
	public Object queryForObject(String sql, Object[] objs, RowMapper<Object> rowMapper) throws DataAccessException {
		if (debug) {
			logger.info("sql:" + sql);
			logger.info("objs:" + objs);
		}
		Object object = null;
		try {
			long time = System.currentTimeMillis();
			object = getJdbcTemplate().queryForObject(sql, objs, rowMapper);
			if (System.currentTimeMillis() - time > 1000 * 60) {
				logger.warn("sql query time is too long............start........");
				logger.warn(sql);
				logger.warn("sql query time is too long............end........");
			}
		} catch (EmptyResultDataAccessException e) {
		}
		return object;
	}

	/**
	 * 
	 * @Description:(通过sql查询列表)
	 * 
	 * @param sql
	 * @return List
	 * 
	 * @author: jessen
	 * @date: 2012-6-1 下午10:10:42
	 * @version 1.0.0
	 * 
	 */
	public List query(String sql) throws DataAccessException {
		if (debug) {
			logger.info("sql:" + sql);
		}
		List ret = null;
		try {
			long time = System.currentTimeMillis();
			ret = getJdbcTemplate().queryForList(sql);
			if (System.currentTimeMillis() - time > 1000 * 60) {
				logger.warn("sql query time is too long............start........");
				logger.warn(sql);
				logger.warn("sql query time is too long............end........");
			}
		} catch (EmptyResultDataAccessException e) {
		}
		return ret;
	}

	/**
	 * 
	 * @Description:(通过sql,rowmapper查询列表)
	 * 
	 * @param sql
	 * @param rowMapper
	 * @return List
	 * 
	 * @author: jessen
	 * @date: 2012-6-1 下午10:10:42
	 * @version 1.0.0
	 * 
	 */
	public List query(String sql, RowMapper rowMapper) throws DataAccessException {
		if (debug) {
			logger.info("sql:" + sql);
		}
		List ret = null;
		try {
			long time = System.currentTimeMillis();
			ret = getJdbcTemplate().query(sql, rowMapper);
			if (System.currentTimeMillis() - time > 1000 * 60) {
				logger.warn("sql query time is too long............start........");
				logger.warn(sql);
				logger.warn("sql query time is too long............end........");
			}
		} catch (EmptyResultDataAccessException e) {
		}
		return ret;
	}

	/**
	 * 
	 * @Description:(通过sql,map参数查询列表)
	 * 
	 * @param sql
	 * @param paramMap
	 * @return List
	 * 
	 * @author: jessen
	 * @date: 2012-6-1 上午12:14:48
	 * @version 1.0.0
	 * 
	 */
	public List query(String sql, Map<String, Object> paramMap) throws DataAccessException {
		if (debug) {
			logger.info("sql:" + sql);
			logger.info("paramMap:" + paramMap);
		}
		List ret = null;
		try {
			long time = System.currentTimeMillis();
			ret = getJdbcTemplate().queryForList(sql, paramMap);
			if (System.currentTimeMillis() - time > 1000 * 60) {
				logger.warn("sql query time is too long............start........");
				logger.warn(sql);
				logger.warn("sql query time is too long............end........");
			}
		} catch (EmptyResultDataAccessException e) {
		}
		return ret;
	}

	/**
	 * 
	 * @Description:(通过sql,对象数组参数查询列表)
	 * 
	 * @param sql
	 * @param objs
	 * @return List
	 * 
	 * @author: jessen
	 * @date: 2012-6-1 上午12:15:20
	 * @version 1.0.0
	 * 
	 */
	public List query(String sql, Object[] objs) throws DataAccessException {
		if (debug) {
			logger.info("sql:" + sql);
			logger.info("objs:" + objs);
		}
		List ret = null;
		try {
			long time = System.currentTimeMillis();
			ret = getJdbcTemplate().queryForList(sql, objs);
			if (System.currentTimeMillis() - time > 1000 * 60) {
				logger.warn("sql query time is too long............start........");
				logger.warn(sql);
				logger.warn("sql query time is too long............end........");
			}
		} catch (EmptyResultDataAccessException e) {
		}
		return ret;
	}

	/**
	 * 
	 * @Description:(通过sql,paramMap,rowMapper查询列表)
	 * 
	 * @param sql
	 * @param paramMap
	 * @param rowMapper
	 * @return List
	 * 
	 * @author: jessen
	 * @date: 2012-6-1 下午10:03:03
	 * @version 1.0.0
	 * 
	 */
	public List query(String sql, Map<String, Object> paramMap, RowMapper rowMapper) throws DataAccessException {
		if (debug) {
			logger.info("sql:" + sql);
			logger.info("paramMap:" + paramMap);
		}
		List ret = null;
		try {
			long time = System.currentTimeMillis();
			ret = getNameParameterJdbcTemplate().query(sql, paramMap, rowMapper);
			if (System.currentTimeMillis() - time > 1000 * 60) {
				logger.warn("sql query time is too long............start........");
				logger.warn(sql);
				logger.warn("sql query time is too long............end........");
			}
		} catch (EmptyResultDataAccessException e) {
		}
		return ret;
	}

	/**
	 * 
	 * @Description:(通过sql,对象数组,rowmapper参数查询列表)
	 * 
	 * @param sql
	 * @param objs
	 * @param rowMapper
	 * @return List
	 * 
	 * @author: jessen
	 * @date: 2012-6-1 上午12:15:35
	 * @version 1.0.0
	 * 
	 */
	public List query(String sql, Object[] objs, RowMapper rowMapper) throws DataAccessException {
		if (debug) {
			logger.info("sql:" + sql);
			logger.info("objs:" + objs);
		}
		List ret = null;
		try {
			long time = System.currentTimeMillis();
			ret = getJdbcTemplate().query(sql, objs, rowMapper);
			if (System.currentTimeMillis() - time > 1000 * 60) {
				logger.warn("sql query time is too long............start........");
				logger.warn(sql);
				logger.warn("sql query time is too long............end........");
			}
		} catch (EmptyResultDataAccessException e) {
		}
		return ret;
	}
}