package com.share.service.system.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.JsonEncoding;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.share.core.dao.system.TestDao;
import com.share.core.framework.BaseService;
import com.share.core.pojos.system.Test;
import com.share.service.system.TestService;

/**
 * 
 * @ClassName:TestServiceImpl
 * @Package: com.share.service.system.impl
 * @Title:(测试类SERVICE服务实现类)
 * @Description:(用一句话描述该文件做什么)
 * 
 * @author: jessen.chen
 * @date: 2012-6-8 下午02:20:09
 * @version 1.0.0
 * 
 */
@Service
public class TestServiceImpl extends BaseService implements TestService {
	private TestDao testDao;

	@Autowired
	public void setTestDao(TestDao testDao) {
		this.testDao = testDao;
	}

	/**
	 * 测试添加
	 */
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public String insertTest(Test test) throws Exception {

		testDao.add(test);
		// 测试回滚
		// test.setUsername("testtesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttestttesttesttesttesttest");
		// testDao.add(test);
		ObjectMapper mapper = new ObjectMapper();
		JsonGenerator jsonGenerator = mapper.getJsonFactory().createJsonGenerator(System.out, JsonEncoding.UTF8);
		// mapper.getJsonFactory().createjsong

		jsonGenerator.writeObject(test);
		System.out.println();
		mapper.writeValue(System.out, test);

		return null;
	}

	public String jsonTest(Test test) {
		logger.info("jsonTest(Test test) start.........................");
		String jsonStr = null;
		try {
			jsonStr = beanToJson(test);
			
			logger.debug("jsonStr:" + jsonStr);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			Test test2 = (Test) jsonToBean(jsonStr, Test.class);

			logger.debug("test2:" + test2);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		logger.info("jsonTest(Test test) end.........................");
		return jsonStr;
	}
}
