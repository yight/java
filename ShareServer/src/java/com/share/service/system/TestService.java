package com.share.service.system;

import com.share.core.pojos.system.Test;

/**
 * 
* @ClassName:TestService
* @Package:  com.share.service.system 
* @Title:(测试类SERVICE服务接口类)
* @Description:(用一句话描述该文件做什么)
* 
* @author: jessen.chen
* @date:   2012-6-8 下午12:50:13
* @version 1.0.0
*
 */
public interface TestService {
	public String insertTest(Test test) throws Exception;
	
	public String jsonTest(Test test);
}
