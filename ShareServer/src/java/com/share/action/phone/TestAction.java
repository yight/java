package com.share.action.phone;

import java.io.Serializable;

import com.share.action.framework.BaseAction;

/**
 * 
 * @ClassName:TestAction
 * @Package: com.share.action.phone
 * @Title:(测试类)
 * @Description:(用一句话描述该文件做什么)
 * 
 * @author: jessen.chen
 * @date: 2012-6-1 下午12:18:37
 * @version 1.0.0
 * 
 */
public class TestAction extends BaseAction implements Serializable {

	/**
	 * serialVersionUID:TODO（用一句话描述这个变量表示什么）
	 * 
	 * @author: jessen.chen
	 * @date: 2012-6-1 下午12:50:57
	 * @version 1.0.0
	 * 
	 */

	private static final long serialVersionUID = -7234309322724921914L;
	private int id;
	public String name;

	/**
	 * 
	 * @Description:(测试提交数据 )
	 * 
	 * @return String
	 * 
	 * @author: jessen.chen
	 * @date: 2012-6-1 下午12:29:09
	 * @version 1.0.0
	 * 
	 */
	public String testSubmit() {
		logger.debug("TestAction............testPost...........start..........");
		logger.debug("ID:" + id);
		logger.debug("NAME:" + name);
		String resultStr = "";
		try {
			ajax(resultStr, CONTENTTYPE_TEXTHTML);
		} catch (Exception e) {
			// TODO: handle exception
			logger.error("testSubmit Exception:" + e.getMessage());
		}
		logger.debug("TestAction............testPost...........end..........");
		return null;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
