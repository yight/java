package com.share.action.framework;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionSupport;

/**
 * 
 * @ClassName:BaseAction
 * @Package: com.share.action.framework
 * @Title:(Action 基类)
 * @Description:(用一句话描述该文件做什么)
 * 
 * @author: jessen.chen
 * @date: 2012-6-1 下午12:17:23
 * @version 1.0.0
 * 
 */
public abstract class BaseAction extends ActionSupport {

	// get log4j handler
	protected static final Logger logger = Logger.getLogger(BaseAction.class);
	// ContentType: text/html
	protected static final String CONTENTTYPE_TEXTHTML = "text/html";

	/**
     * AJAX输出，返回null
     *
     * @param content - 输出内容
     * @return type - 输出类型
     */
	public String ajax(String content, String type) {
		try {
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType(type + ";charset=UTF-8");
			response.setHeader("Pragma", "No-cache");
			response.setHeader("Cache-Control", "no-cache");
			response.setDateHeader("Expires", 0);
			response.getWriter().write(content);
			response.getWriter().flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
}
